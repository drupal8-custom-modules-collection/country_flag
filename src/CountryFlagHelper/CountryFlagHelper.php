<?php

namespace Drupal\country_flag\CountryFlagHelper;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;

/**
 * Implements CountryFlagHelper form.
 */
class CountryFlagHelper {

  const COUNTRIES_URL = 'https://restcountries.eu/rest/v2/all?fields=name';
  const COUNTRY_URL = 'https://restcountries.eu/rest/v2/name/';

  /**
   * Connect to the endpoint and get countries' names.
   *
   * @return array
   */
  public function getCountries() {
    $countries_values = $this->getResult(self::COUNTRIES_URL);
    foreach ($countries_values as $country) {
      $countries[$country['name']] = $country['name'];
    }

    return $countries;
  }

  /**
   * Connect to the endpoint and get country's flag.
   *
   * @param string $country_name
   *
   * @return string
   */
  public function getFlag($country_name) {
    $country_name = UrlHelper::encodePath($country_name);
    $country = $this->getResult(self::COUNTRY_URL . $country_name . '?fields=flag');

    return $country[0]['flag'];

  }

  /**
   * Connect to the endpoint and get the result.
   *
   * @param string $url
   *
   * @return array or null
   */
  public function getResult($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);

    if (curl_error($ch)) {
      \Drupal::logger('country_flag')->notice(curl_error($ch));
    }
    curl_close($ch);

    return Json::decode($result);
  }

}
