<?php

namespace Drupal\country_flag\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Country Flag Form' block.
 *
 * @Block(
 *  id = "country_flag_block",
 *  admin_label = @Translation("Country flag block"),
 * )
 */
class CountryFlagBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $form = \Drupal::formBuilder()->getForm('Drupal\country_flag\Form\CountryFlagForm');
    $rendered_form = \Drupal::service('renderer')->render($form);
    return ['#markup' => $rendered_form];
  }

}
